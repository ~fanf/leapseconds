#!/usr/bin/perl

use warnings;
use strict;

use POSIX;
use Time::HiRes qw(time sleep);

my %tostr = (
	bin => "%b",
	oct => "%o",
	dec => "%d",
	hex => "%x",
    );
my %tonum = (
	bin => sub { oct "0b$_[0]" },
	oct => sub { oct $_[0] },
	dec => sub { 0 + $_[0] },
	hex => sub { hex $_[0] },
    );
my %user = (
	bin => 'time_b_emit',
	oct => 'time_o_emit',
	dec => 'time_t_emit',
	hex => 'time_x_emit',
    );

my $base = $ARGV[0];
unless (defined $base and exists $user{$base}) {
	printf STDERR "usage: $0 %s [time]\n", join "|", sort keys %user;
	exit 1;
}

sub tostr { sprintf $tostr{$base}, @_ }
sub tonum { $tonum{$base}->(@_) }

sub fmt {
	return strftime "%F.%T", gmtime $_[0];
}

sub palindrome {
	my $str = shift;
	my $len = length $str;
	my $half = substr $str, 0, ($len + 1)/2;
	my $mid = chop $half;
	return $half . ($len & 1 ? $mid : $mid.$mid) . reverse $half;
}

my $test = defined $ARGV[1];
my $t = $test && $ARGV[1] ne 'now' ? tonum $ARGV[1] : time;

my $pal = palindrome tostr $t;
my $p = tonum $pal;
my $i = $p - $t;

exit unless $test or (0 < $i and $i < 60);

my $fmt = fmt $p;

printf "%s %s\n", tostr($t), fmt($t) if $i > 0;
printf "%s %s %f\n", $pal, $fmt, $i;
printf "%s %s\n", tostr($t), fmt($t) if $i < 0;

exit if $test;

sleep $p - time;
exec "$ENV{HOME}/bin/tweet $user{$base} $pal $fmt";
