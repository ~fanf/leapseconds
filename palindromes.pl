#!/usr/bin/perl

use warnings;
use strict;

use POSIX;

sub fmt {
	return strftime "%F.%T", gmtime $_[0];
}
sub hms {
	return strftime "%T", gmtime $_[0];
}

sub halve {
	my $str = shift;
	my $len = length $str;
	return $len & 1, substr $str, 0, ($len + 1)/2;
}

sub mirror {
	my $odd = shift;
	my $str = shift;
	my $mid = chop $str;
	my $rev = reverse $str;
	return $str . ($odd ? $mid : $mid.$mid) . $rev;
}

my %tostr = (
	bin => "%b",
	oct => "%o",
	dec => "%d",
	hex => "%x",
    );
my %tonum = (
	bin => sub { oct "0b$_[0]" },
	oct => sub { oct $_[0] },
	dec => sub { 0 + $_[0] },
	hex => sub { hex $_[0] },
    );

my $base = $ARGV[0];
unless (defined $base and exists $tostr{$base}) {
	printf STDERR "usage: palindromes.pl %s\n", join "|", sort keys %tostr;
	exit 1;
}

sub tostr { sprintf $tostr{$base}, @_ }
sub tonum { $tonum{$base}->(@_) }

my $t = time;

my ($odd,$top) = halve tostr $t;
my $ntop = tonum $top;
my $b = tonum mirror $odd, tostr $ntop;
$b = tonum mirror $odd, tostr --$ntop if $b > $t;

for my $i (-9 .. 0) {
	my $pal = mirror $odd, tostr $ntop + $i;
	printf "%s %s\n", $pal, fmt tonum $pal;
}

printf "%*s\n", length(tostr $t) + 20, hms($t - $b);
printf "%s %s\n", tostr($t), fmt $t;
$ntop += 1;
my $a = tonum mirror $odd, tostr $ntop;
printf "%*s\n", length(tostr $t) + 20, hms($a - $t);

for my $i (0 .. 9) {
	my $pal = mirror $odd, tostr $ntop + $i;
	printf "%s %s\n", $pal, fmt tonum $pal;
}
